from __future__ import unicode_literals, print_function

from tkinter import ttk
import subprocess
import sys
import tkinter

from wow_realmlist_switcher import launcher, widgets


class Main:

    def __init__(self, parent, settings, storage, wow_realmlist):
        self.__parent = parent
        self.__settings = settings
        self.__storage = storage
        self.__wow_realmlist = wow_realmlist

        self.__build()
        self.__apply_style()

    def __build(self):
        body = ttk.Frame(self.__parent, padding='3 3 12 12')
        body.grid(column=0, row=0, sticky=(
            tkinter.N, tkinter.S, tkinter.W, tkinter.E))
        body.columnconfigure(0, weight=1)
        body.rowconfigure(0, weight=1)

        self.__body = body

        self.__create_realmlist_selector()

        self.__run_button = widgets.RunWow(
            parent=self.__body, callback=self.__run_wow)
        self.__run_button.grid(column=10, row=5)

    def __create_realmlist_selector(self):
        current = str(self.__wow_realmlist.current)
        names = {
            str(item)
            for item in self.__storage.realmlists
            if not str(item) == str(current)
        }
        names.add(current)

        widget = widgets.SelectRealmlist(
            parent=self.__body, values=list(names), current=current)
        widget.grid(column=5, row=5)

        self.__realmlist_selector = widget

    def __apply_style(self):

        for widget in self.__body.winfo_children():
            widget.grid_configure(padx=10, pady=10)

    def __run_wow(self):
        selected_name = self.__realmlist_selector.selected

        for item in self.__storage.realmlists:
            if item.name == selected_name:
                realmlist = item

        self.__wow_realmlist.set(realmlist.uri)
        launcher.run()

