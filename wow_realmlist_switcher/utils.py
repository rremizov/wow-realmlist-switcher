from concurrent.futures import ThreadPoolExecutor


def instantiate(*args, **kwargs):
    """
    Use it as a class decorator to create instance of the class.
    Arguments of decorator are passed to class' `__init__` method.

    >>> @instantiate(greeting='Hello!')
    ... class A:
    ...     def __init__(self, *args, **kwargs):
    ...         print(kwargs['greeting'])
    Hello!
    """

    def wrapper(class_):
        return class_(*args, **kwargs)

    return wrapper


def thread(max_workers=1):
    """
    Use it as a method or function decorator to move execution into separate
    thread.

    :returns: concurrent.futures.Future -- result of decorated callable

    >>> import time
    >>> import concurrent.futures

    >>> class A:
    ...     @thread()
    ...     def greet(self, arg0, arg1):
    ...         time.sleep(1)
    ...         print(arg0)
    ...         return arg1

    >>> future = A().greet('Hello!', 'Awesome!')
    >>> future.done()
    False
    >>> print('-------')
    -------
    >>> _ = concurrent.futures.wait([future])
    Hello!
    >>> future.result()
    'Awesome!'
    """

    def decorator(function):

        def wrapper(*args, **kwargs):
            executor = ThreadPoolExecutor(max_workers=max_workers)
            future = executor.submit(function, *args, **kwargs)
            executor.shutdown(wait=False)
            return future

        return wrapper

    return decorator


if __name__ == '__main__':
    import doctest
    doctest.testmod()

