from __future__ import unicode_literals, print_function

from tkinter import ttk
import tkinter


class SelectRealmlist:

    def __init__(self, parent, values, current=None):
        self.__parent = parent
        self.__values = values
        self.__current = current

        self.__build()

    def __build(self):
        selected = tkinter.StringVar()
        body = ttk.Combobox(
            self.__parent, textvariable=selected, state='readonly')
        body['values'] = self.__values

        if self.__current is not None:
            selected.set(self.__current)

        self.__selected = selected
        self.__body = body

    @property
    def selected(self):
        return self.__selected.get()

    def grid(self, *args, **kwargs):
        self.__body.grid(*args, **kwargs)


class RunWow:

    def __init__(self, parent, callback):
        self.__parent = parent
        self.__callback = callback

        self.__build()

    def __build(self):
        body = ttk.Button(self.__parent, text='Run', command=self.__callback)

        self.__body = body

    def grid(self, *args, **kwargs):
        self.__body.grid(*args, **kwargs)

