#!/usr/bin/env python3 -O
# encoding: utf-8

from wow_realmlist_switcher.application import Application


def main():
    Application().run()


if __name__ == '__main__':
    main()

