from __future__ import unicode_literals, print_function

import argparse
import json
import logging
import os
import tkinter

from wow_realmlist_switcher import frames, utils


@utils.instantiate()
class Settings:
    theme = 'clam'

    def __init__(self, *args, **kwargs):
        self.__cli = None

    @property
    def cli(self):
        if self.__cli is not None:
            return self.__cli

        self.__cli = self._get_cl_arguments()
        return self.__cli

    def _get_cl_arguments(self):
        parser = argparse.ArgumentParser(
            description='Manage WoW reallists')

        parser.add_argument(
            '-s', '--storage', default='wow-realmlist-switcher.json',
            help='JSON file with realmlists data')

        return parser.parse_args()


class Application:

    def __init__(self):
        self.__frames = dict()

        root = tkinter.Tk()
        root.title('WoW Realmlist Switcher')
        root.resizable(True, True)
        root.columnconfigure(0, weight=1)
        root.rowconfigure(0, weight=1)
        self.__root = root

        self.__build()
        self.__apply_style()
        self.__bind_hotkeys()

    def __build(self):
        self.__frames['main'] = frames.Main(
            parent=self.__root,
            settings=Settings, storage=Storage, wow_realmlist=StoredRealmlist)

    def __apply_style(self):

        theme = Settings.theme

        if theme in tkinter.ttk.Style().theme_names():
            tkinter.ttk.Style().theme_use(theme)

        else:
            self.logger.warning('theme %s is unavailable', theme)

    def __bind_hotkeys(self):
        self.__root.bind('<Control-q>', lambda *argv: self.__root.quit())

    @property
    def logger(self):
        logger = logging.getLogger(__name__)
        logger.setLevel(logging.DEBUG)

        handler = logging.StreamHandler()
        handler.setFormatter(logging.Formatter(
            '%(asctime)s;%(name)s;%(levelname)s;%(message)s'))
        logger.addHandler(handler)

        self.__logger = logger
        return logger

    def run(self):
        self.__root.mainloop()


class ThreadedFileIO:

    def __init__(self, path):
        self.__path = path
        self.__content = self.__read()

    @utils.thread()
    def __read(self):
        with open(self.__path) as file_:
            return file_.read()

    @utils.thread()
    def __write(self, content):
        with open(self.__path, 'w') as file_:
            print(content, file=file_)

        self.__content = self.__read()

    def get(self):
        return self.__content.result()

    def set(self, value):
        return self.__write(value)



@utils.instantiate(Settings.cli.storage)
class Storage(ThreadedFileIO):

    @property
    def realmlists(self):
        return [
            Realmlist(item)
            for item in json.loads(self.get())['realmlists']
        ]

    def new_realmlist(self, realmlist):
        storage = json.loads(self.get())
        storage['realmlists'].append(realmlist.dict())
        self.set(json.dumps(storage, indent=4))


@utils.instantiate(os.path.join(os.getcwd(), 'Data', 'ruRU', 'realmlist.wtf'))
class StoredRealmlist(ThreadedFileIO):

    @property
    def current(self):
        current_uri = self.get().split(' ')[-1].strip()

        for realmlist in Storage.realmlists:
            if realmlist.uri == current_uri:
                return realmlist

        new = Realmlist(dict(name=current_uri, uri=current_uri))
        Storage.new_realmlist(new)
        return new

    def set(self, value):
        super().set('set realmlist {}'.format(value)).result()


class Realmlist:
    def __init__(self, dict_):
        self.__name = dict_['name']
        self.__uri = dict_['uri']

    def __str__(self):
        return self.__name

    def __repr__(self):
        return '{}:{}'.format(self.__name, self.__uri)

    @property
    def name(self):
        return self.__name

    @property
    def uri(self):
        return self.__uri

    def dict(self):
        return dict(name=self.__name, uri=self.__uri)

