from setuptools import setup
import os


def _parse_requirements(filename):
    result = set()
    path = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        'requirements',
        filename)

    for line in open(path):
        line = line.strip()

        if not line:
            continue

        if line.startswith('-r'):
            result = result.union(_parse_requirements(line.split(' ')[1]))

        else:
            result.add(line)

    return result


setup(
    name='wow-realmlist-switcher',
    version='0.0.2',
    author='Roman M. Remizov',
    author_email='rremizov@yandex.ru',

    license='MIT',
    platforms=('any', ),
    description='',
    long_description=open('README.md').read(),
    url='http://github.com/rremizov/wow-realmlist-switcher',

    entry_points = {
        'gui_scripts': [
            'wow_realmlist_switcher = wow_realmlist_switcher.main:main',
        ],
    },

    packages=(
        'wow_realmlist_switcher',
    ),
    install_requires=_parse_requirements('setup.txt'),

    test_suite='tests.run_tests.run_all',
    tests_require=_parse_requirements('dev.txt'),

    classifiers=(
        'Development Status :: 5 - Production/Stable',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        # 'Programming Language :: Python :: 2',
        # 'Programming Language :: Python :: 2.6',
        # 'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.2',
        # 'Programming Language :: Python :: 3.3',
        # 'Programming Language :: Python :: 3.4',
    ),

    # include_package_data=True,
)

